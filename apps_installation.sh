#!/bin/bash

sudo apt update
sudo apt install -y lsb-release ca-certificates apt-transport-https software-properties-common gnupg2

sudo echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/sury-php.list

sudo wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -

sudo apt update

sudo apt-get install php8.1 -y

sudo apt-get install php8.1-fpm -y

sudo apt-get install php8.1-{bcmath,bz2,intl,gd,mbstring,mysql,zip,common} -y

sudo apt install php php-cli php-fpm php-json php-common php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath -y

# Install composer programatically
wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet

sudo mv composer.phar /usr/bin/composer

sudo apt-get install mariadb-server mariadb-client -y

# Install redis
sudo apt-get install redis-server -y
sudo systemctl enable redis-server

sudo apt install git -y
